package lt.mesgalis.kargis.ui;

/**
 * Created by Karolis Jocevičius
 */
public enum GeomType {
    POINT, LINE, POLYGON
}
