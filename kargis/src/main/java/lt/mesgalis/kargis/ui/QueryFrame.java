package lt.mesgalis.kargis.ui;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import org.geotools.data.FeatureSource;
import org.geotools.data.Query;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.FeatureIterator;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.swing.action.SafeAction;
import org.geotools.swing.table.FeatureCollectionTableModel;
import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.type.FeatureType;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.identity.FeatureId;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Set;

/**
 * Created by Karolis Jocevičius
 */
public class QueryFrame extends JFrame {

    private final FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
    private final MainFrame mainFrame;
    //    private JComboBox featureTypeCBox;
    private JTable table;
    private JTextField text;

    public QueryFrame(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        initGUI();
    }

    private void initGUI() {
        this.setTitle("Query");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        getContentPane().setLayout(new BorderLayout());

        text = new JTextField(80);
        text.setText("include"); // include selects everything!
        text.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    filterFeatures(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        getContentPane().add(text, BorderLayout.NORTH);

        table = new JTable();
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setModel(new DefaultTableModel(5, 5));
        table.setPreferredScrollableViewportSize(new Dimension(500, 200));

        JScrollPane scrollPane = new JScrollPane(table);
        getContentPane().add(scrollPane, BorderLayout.CENTER);

        JMenuBar menubar = new JMenuBar();
        setJMenuBar(menubar);

        JMenu dataMenu = new JMenu("Data");
        menubar.add(dataMenu);
        pack();

        // docs start data menu
        dataMenu.add(new SafeAction("Get features") {
            public void action(ActionEvent e) throws Throwable {
                filterFeatures(true);
            }
        });
        dataMenu.add(new SafeAction("Count") {
            public void action(ActionEvent e) throws Throwable {
                countFeatures();
            }
        });
        dataMenu.add(new SafeAction("Geometry") {
            public void action(ActionEvent e) throws Throwable {
                queryFeatures();
            }
        });
        // docs end data menu
        dataMenu.add(new SafeAction("Center") {
            public void action(ActionEvent e) throws Throwable {
                centerFeatures();
            }
        });
    }

    public void showSelection(FeatureSource featureSource, Set<FeatureId> ids) {
        Filter filter = ff.id(ids);
        SimpleFeatureCollection features = null;
        try {
            features = (SimpleFeatureCollection) featureSource.getFeatures(filter);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FeatureCollectionTableModel model = new FeatureCollectionTableModel(features);
        table.setModel(model);
    }

//    private void updateUI() throws Exception {
//        ComboBoxModel cbm = new DefaultComboBoxModel(dataStore.getTypeNames());
//        featureTypeCBox.setModel(cbm);
//
//        table.setModel(new DefaultTableModel(5, 5));
//    }

    private void filterFeatures() throws Exception {
        filterFeatures(false);
    }

    private void filterFeatures(boolean local) throws Exception {
        FeatureSource source = mainFrame.getSelectedLayer().getFeatureSource();

        Filter filter = CQL.toFilter(text.getText());
        SimpleFeatureCollection features = (SimpleFeatureCollection) source.getFeatures(filter);
        FeatureCollectionTableModel model = new FeatureCollectionTableModel(features);

        if (local) {
            mainFrame.getSelectedIds().clear();
            FeatureIterator iter = features.features();
            try {
                while (iter.hasNext()) {
                    Feature feature = iter.next();
                    mainFrame.getSelectedIds().add(feature.getIdentifier());

                    System.out.println("   " + feature.getIdentifier());
                }

            } finally {
                iter.close();
                mainFrame.displaySelectedFeatures(mainFrame.getSelectedIds());
            }
        }

        table.setModel(model);
    }

    private void countFeatures() throws Exception {
        FeatureSource source = mainFrame.getSelectedLayer().getFeatureSource();

        Filter filter = CQL.toFilter(text.getText());
        SimpleFeatureCollection features = (SimpleFeatureCollection) source.getFeatures(filter);

        int count = features.size();
        JOptionPane.showMessageDialog(text, "Number of selected features:" + count);
    }

    private void queryFeatures() throws Exception {
        FeatureSource source = mainFrame.getSelectedLayer().getFeatureSource();

        FeatureType schema = source.getSchema();
        String name = schema.getGeometryDescriptor().getLocalName();

        Filter filter = CQL.toFilter(text.getText());

        Query query = new Query(source.getName().getLocalPart(), filter, new String[]{name});

        SimpleFeatureCollection features = (SimpleFeatureCollection) source.getFeatures(query);

        FeatureCollectionTableModel model = new FeatureCollectionTableModel(features);
        table.setModel(model);
    }

    private void centerFeatures() throws Exception {
        FeatureSource source = mainFrame.getSelectedLayer().getFeatureSource();

        Filter filter = CQL.toFilter(text.getText());

        FeatureType schema = source.getSchema();
        String name = schema.getGeometryDescriptor().getLocalName();
        Query query = new Query(source.getName().getLocalPart(), filter, new String[]{name});

        SimpleFeatureCollection features = (SimpleFeatureCollection) source.getFeatures(filter);

        double totalX = 0.0;
        double totalY = 0.0;
        long count = 0;
        SimpleFeatureIterator iterator = features.features();
        try {
            while (iterator.hasNext()) {
                SimpleFeature feature = iterator.next();
                Geometry geom = (Geometry) feature.getDefaultGeometry();
                com.vividsolutions.jts.geom.Point centroid = geom.getCentroid();
                totalX += centroid.getX();
                totalY += centroid.getY();
                count++;
            }
        } finally {
            iterator.close(); // IMPORTANT
        }
        double averageX = totalX / (double) count;
        double averageY = totalY / (double) count;
        Coordinate center = new Coordinate(averageX, averageY);

        JOptionPane.showMessageDialog(text, "Center of selected features:" + center);
    }
}
