package lt.mesgalis.kargis.ui.actions;

import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.styling.SLD;
import org.geotools.styling.Style;
import org.geotools.swing.data.JFileDataStoreChooser;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

/**
 * Adds layer to MapContent from file
 *
 * Created by Karolis Jocevičius
 */
public class AddLayerAction extends AbstractAction {
    private static final long serialVersionUID = 963997311827400054L;

    private MapContent content;

    public AddLayerAction(MapContent content) {
        this.setContent(content);
    }

    public void actionPerformed(ActionEvent arg0) {
        // display a data store file chooser dialog for shapefiles
        File file = JFileDataStoreChooser.showOpenFile("shp", new File("resource"), null);
        if (file == null) {
            return;
        }

        FileDataStore store = null;
        SimpleFeatureSource featureSource = null;
        try {
            store = FileDataStoreFinder.getDataStore(file);
            featureSource = store.getFeatureSource();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        Style style = SLD.createSimpleStyle(featureSource.getSchema());
        Layer layer = new FeatureLayer(featureSource, style);
        content.addLayer(layer);
    }

    void setContent(MapContent content) {
        this.content = content;
    }

}
