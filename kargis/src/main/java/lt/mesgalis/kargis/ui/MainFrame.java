package lt.mesgalis.kargis.ui;

import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import lt.mesgalis.kargis.KarGISUtils;
import lt.mesgalis.kargis.ui.actions.AddLayerAction;
import lt.mesgalis.kargis.ui.actions.T2CalculationsAction;
import lt.mesgalis.kargis.ui.actions.T3CalculationsAction;
import lt.mesgalis.kargis.ui.cursor.CreateLineLayerCursorTool;
import lt.mesgalis.kargis.ui.cursor.MultiSelectCursorTool;
import org.geotools.data.FeatureSource;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.FeatureCollection;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.styling.*;
import org.geotools.swing.JMapFrame;
import org.opengis.feature.type.GeometryDescriptor;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.identity.FeatureId;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Set;

/**
 * Created by Karolis Jocevičius
 * <p/>
 * Main window. Gives access to all KarGIS functions
 */
public class MainFrame extends JMapFrame {
    private static final long serialVersionUID = 6127096856069059666L;

    /*
     * Some default style variables
     */
    private static final Color LINE_COLOUR = Color.BLACK;
    private static final Color FILL_COLOUR = Color.DARK_GRAY;
    private static final Color SELECTED_COLOUR = Color.RED;
    private static final float OPACITY = 1.0f;
    private static final float LINE_WIDTH = 1.0f;
    private static final float POINT_SIZE = 10.0f;

    /*
     * Factories that we will use to create style and filter objects
     */
    private final FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
    private final StyleFactory sf = CommonFactoryFinder.getStyleFactory();
    private final QueryFrame queryFrame;
    private GeomType geometryType;
    private String geometryAttributeName;
    private Set<FeatureId> selectedIds;

    public MainFrame(MapContent map) {
        super(map);
        setSize(800, 600);

        //Enable components
        enableLayerTable(true);
        enableToolBar(true);
        enableStatusBar(true);
        enableTool(Tool.values());
        initComponents();

        initButtons();
        queryFrame = new QueryFrame(this);

    }

    public GeomType getGeometryType() {
        return geometryType;
    }

    public void setGeometryType(GeomType geometryType) {
        this.geometryType = geometryType;
    }

    /**
     * Add custom toolbar buttons
     */
    private void initButtons() {
        JButton button;

        //Add layer
        button = new JButton(new AddLayerAction(getMapContent()));
        button.setText("Add layer");
        getToolBar().add(button);

        //Multiselect
        button = new JButton();
        button.setText("Select");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
//                resetSelections();
                getMapPane().setCursorTool(new MultiSelectCursorTool(MainFrame.this));
            }
        });
        getToolBar().add(button);

        //Query window
        button = new JButton();
        button.setText("Query");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                queryFrame.setVisible(true);
            }
        });
        getToolBar().add(button);

        //Show selected
        button = new JButton();
        button.setText("Show selected");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                zoomToSelection();
            }
        });
        getToolBar().add(button);

        //Save selected
        button = new JButton();
        button.setText("Save selected");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    FeatureCollection features = getSelectedLayer().getFeatureSource().getFeatures().subCollection(ff.id(selectedIds));
                    KarGISUtils.getInstance().createNewFS((SimpleFeatureCollection) features);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        getToolBar().add(button);

        //Draw line
        button = new JButton();
        button.setText("Create line");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getMapPane().setCursorTool(new CreateLineLayerCursorTool(getMapPane()));
            }
        });
        getToolBar().add(button);

        //T2 calculations
        button = new JButton();
        button.setText("T2");
        button.addActionListener(new T2CalculationsAction(MainFrame.this));
        getToolBar().add(button);

        //T3
        button = new JButton();
        button.setText("T3");
        button.addActionListener(new T3CalculationsAction(this));
        getToolBar().add(button);
    }

    public Layer getSelectedLayer() {
        for (Layer layer : getMapPane().getMapContent().layers()) {
            if (layer.isSelected()) {
                return layer;
            }
        }
        return null;
    }

    void zoomToSelection() {
        if (selectedIds == null) {
            return;
        }

        Filter filter = ff.id(selectedIds);
        SimpleFeatureCollection features = null;
        try {
            features = (SimpleFeatureCollection) getSelectedLayer().getFeatureSource().getFeatures(filter);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (features == null) {
            return;
        }
        ReferencedEnvelope selectedArea = features.getBounds();

        getMapPane().setDisplayArea(selectedArea);
    }

    /**
     * Sets the display to paint selected features yellow and
     * unselected features in the default style.
     */
    public void displaySelectedFeatures() {
        displaySelectedFeatures(getSelectedIds());
    }

    /**
     * Sets the display to paint selected features yellow and
     * unselected features in the default style.
     *
     * @param IDs identifiers of currently selected features
     */
    public void displaySelectedFeatures(Set<FeatureId> IDs) {
        Style style;

        if (IDs.isEmpty()) {
            style = createDefaultStyle();
        } else {
            style = createSelectedStyle(IDs);
        }

        Layer layer = getSelectedLayer();
        ((FeatureLayer) layer).setStyle(style);
        getMapPane().repaint();
    }

    public void resetSelections() {
        Style style = createDefaultStyle();
        while (getMapPane().getMapContent().layers().iterator().hasNext()) {
            ((FeatureLayer) getMapPane().getMapContent().layers().iterator().next()).setStyle(style);
        }
    }

    /**
     * Create a default Style for feature display
     */
    Style createDefaultStyle() {
        Rule rule = createRule(LINE_COLOUR, FILL_COLOUR);

        FeatureTypeStyle fts = sf.createFeatureTypeStyle();
        fts.rules().add(rule);

        Style style = sf.createStyle();
        style.featureTypeStyles().add(fts);
        return style;
    }

    /**
     * Create a Style where features with given ids are painted
     * red, while others are painted with the default colors.
     */
    private Style createSelectedStyle(Set<FeatureId> IDs) {
        Rule selectedRule = createRule(SELECTED_COLOUR, SELECTED_COLOUR);
        selectedRule.setFilter(ff.id(IDs));

        Rule otherRule = createRule(LINE_COLOUR, FILL_COLOUR);
        otherRule.setElseFilter(true);

        FeatureTypeStyle fts = sf.createFeatureTypeStyle();
        fts.rules().add(selectedRule);
        fts.rules().add(otherRule);

        Style style = sf.createStyle();
        style.featureTypeStyles().add(fts);
        return style;
    }

    /**
     * Helper for createXXXStyle methods. Creates a new Rule containing
     * a Symbolizer tailored to the geometry type of the features that
     * we are displaying.
     */
    private Rule createRule(Color outlineColor, Color fillColor) {
        Symbolizer symbolizer = null;
        Fill fill;
        org.geotools.styling.Stroke stroke = sf.createStroke(ff.literal(outlineColor), ff.literal(LINE_WIDTH));

        switch (geometryType) {
            case POLYGON:
                fill = sf.createFill(ff.literal(fillColor), ff.literal(OPACITY));
                symbolizer = sf.createPolygonSymbolizer(stroke, fill, geometryAttributeName);
                break;

            case LINE:
                symbolizer = sf.createLineSymbolizer(stroke, geometryAttributeName);
                break;

            case POINT:
                fill = sf.createFill(ff.literal(fillColor), ff.literal(OPACITY));

                Mark mark = sf.getCircleMark();
                mark.setFill(fill);
                mark.setStroke(stroke);

                Graphic graphic = sf.createDefaultGraphic();
                graphic.graphicalSymbols().clear();
                graphic.graphicalSymbols().add(mark);
                graphic.setSize(ff.literal(POINT_SIZE));

                symbolizer = sf.createPointSymbolizer(graphic, geometryAttributeName);
        }

        Rule rule = sf.createRule();
        rule.symbolizers().add(symbolizer);
        return rule;
    }


    /**
     * Retrieve information about the feature geometry
     */
    public void setGeometry(FeatureSource featureSource) {
        GeometryDescriptor geomDesc = featureSource.getSchema().getGeometryDescriptor();
        geometryAttributeName = geomDesc.getLocalName();

        Class<?> clazz = geomDesc.getType().getBinding();

        if (com.vividsolutions.jts.geom.Polygon.class.isAssignableFrom(clazz) ||
                MultiPolygon.class.isAssignableFrom(clazz)) {
            geometryType = GeomType.POLYGON;

        } else if (LineString.class.isAssignableFrom(clazz) ||
                MultiLineString.class.isAssignableFrom(clazz)) {

            geometryType = GeomType.LINE;

        } else {
            geometryType = GeomType.POINT;
        }

    }

    public void addLayer(Layer layer) {
        getMapPane().getMapContent().addLayer(layer);
    }

    public QueryFrame getQueryFrame() {
        return queryFrame;
    }

    public String getGeometryAttributeName() {
        return geometryAttributeName;
    }

    public Set<FeatureId> getSelectedIds() {
        return selectedIds;
    }

    public void setSelectedIds(Set<FeatureId> selectedIds) {
        this.selectedIds = selectedIds;
    }
}
