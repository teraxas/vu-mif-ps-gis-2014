package lt.mesgalis.kargis.ui;

import javax.swing.*;
import java.awt.*;

/**
 * Simple text output
 *
 * Created by Karolis Jocevičius
 */
public class OutputFrame extends JFrame {

    private JPanel contentPane;
    private JTextArea textArea;

    public OutputFrame() throws HeadlessException {
        initUI();
    }

    public void print(String msg) {
        setVisible(true);
        textArea.append(msg + "\n");
    }

    public void print(Object sender, String msg) {
        print(sender.getClass().getSimpleName() + ": " + msg);
    }

    private void initUI() {
        setName("Output");
        setTitle("Output");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(400, 400, 630, 300);
        setResizable(false);

        contentPane = new JPanel();
        contentPane.setBounds(0, 0, 620, 300);
        this.add(contentPane);

        textArea = new JTextArea();
        textArea.setBounds(contentPane.getBounds());
        textArea.setAutoscrolls(true);
        textArea.setRows(20);
        textArea.setEditable(false);
        textArea.setLineWrap(true);
        textArea.setMinimumSize(new Dimension(620, 290));
        contentPane.add(new JScrollPane(textArea));
    }

}
