package lt.mesgalis.kargis.ui.cursor;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import lt.mesgalis.kargis.KarGISFeatureFactory;
import lt.mesgalis.kargis.KarGISUtils;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.swing.MapPane;
import org.geotools.swing.event.MapMouseEvent;
import org.geotools.swing.tool.CursorTool;
import org.opengis.feature.simple.SimpleFeature;

import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Allows drawing connected linestrings - rings.
 * After drawing - creates layer and allows saving result to *.shp
 *
 * Created by Karolis Jocevičius
 */
public class CreateLineLayerCursorTool extends CursorTool {

    private GeometryFactory gf = new GeometryFactory();

    private MapPane mapPane;

    private List<DirectPosition2D> points;

    public CreateLineLayerCursorTool(MapPane mapPane) {
        this.mapPane = mapPane;
        this.points = new ArrayList<DirectPosition2D>();
    }

    @Override
    public void onMouseClicked(MapMouseEvent ev) {
        KarGISFeatureFactory.getInstance().changeCRSIfNeeded(mapPane.getMapContent().getCoordinateReferenceSystem());
        if (ev.getButton() == MouseEvent.BUTTON2) {
            SimpleFeatureCollection collection = complete();
            mapPane.getMapContent().addLayer(KarGISUtils.getInstance().createLayer(collection));
            try {
                double length = KarGISUtils.getInstance().getSum(collection, "length");
                KarGISUtils.getInstance().displayMessage("Line length = " + String.valueOf(length));
                KarGISUtils.getInstance().createNewFS(collection);
            } catch (IOException e) {
                e.printStackTrace();
                KarGISUtils.getInstance().displayMessage("Klaida saugant failą!");
            }
        }

        KarGISUtils.getInstance().output("Point at: " + ev.getMapPosition());
        points.add(ev.getWorldPos());
    }

    public SimpleFeatureCollection complete() {
        List<Coordinate> coordinates = new ArrayList<Coordinate>();

        for (DirectPosition2D point : points) {
            coordinates.add(new Coordinate(point.getX(), point.getY()));
        }

        Coordinate[] coordArray = new Coordinate[coordinates.size() + 1];
        for (int i = 0; i < coordinates.size(); i++) {
            coordArray[i] = coordinates.get(i);
        }
        coordArray[coordinates.size()] = coordinates.get(0);

        Geometry geometry = gf.createLinearRing(coordArray);

        List<SimpleFeature> simpleFeatures = KarGISUtils.getInstance().geomToFeatures(new ArrayList<SimpleFeature>(), geometry);
        return new ListFeatureCollection(simpleFeatures.get(0).getFeatureType(), simpleFeatures);

    }

}