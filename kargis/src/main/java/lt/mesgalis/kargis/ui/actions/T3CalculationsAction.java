package lt.mesgalis.kargis.ui.actions;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.util.Stopwatch;
import lt.mesgalis.kargis.KarGISFeatureFactory;
import lt.mesgalis.kargis.KarGISUtils;
import lt.mesgalis.kargis.model.T3Request;
import lt.mesgalis.kargis.ui.MainFrame;
import lt.mesgalis.kargis.ui.T3SettingsFrame;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.filter.text.cql2.CQLException;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.grid.Grids;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Karolis Jocevičius
 *
 *
 · + Buferis aplink parinktus kelius, šalia kurių bus trasa. A pločio. +
 · + Buferis aplink miestus. B skersmens. +
 · + Skirtumas tarp kelių buferio poligono ir miestų poligonų – „pakelės“
 · + Išrenkami HIDRO_L objektai, platesni nei C +
 · + Išrinkti HIDRO_L objektai buferizuojami pagal plotį +
 · + Vandens telkinių sąjunga su buferizuotu HIDRO_L - pavadinkim tai vandens sluoksniu +
 ·  „pakelių“ skirtumas su vandens sluoksniu
 · + Gauto ploto skirtumas su buferizuotu viršukalnių sluoksniu
 · + Surasti pakankamai didelius plotus trasai (bent D ploto)
 · + Leisti vartotojui nubrėžti trasą ir išsaugoti kaip .shp
 *
 */
public class T3CalculationsAction extends AbstractAction {

    public static final String RES_KELIAI_URI = "resource/T3res/KELIAI.shp";
    public static final String RES_HIDRO_L_URI = "resource/T3res/HIDRO_L.shp";
    public static final String RES_GYVENVIETES_URI = "resource/T3res/VIETOV_U.shp";
    public static final String RES_VIRSUKALNES_URI = "resource/T3res/virsukal.shp";
    public static final String RES_PLOTAI_URI = "resource/T3res/PLOTAI.shp";

    private final FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();

    private MainFrame mainFrame;
    private final KarGISUtils utils;

    private T3SettingsFrame settingsFrame;

    private T3Request request;
    private SimpleFeatureSource fsPlotai;
    private SimpleFeatureSource fsGyvenvietes;
    private SimpleFeatureSource fsKeliai;
    private SimpleFeatureSource fsHidroL;
    private SimpleFeatureSource fsVirsukalnes;

    private SimpleFeatureCollection colUpes;
    private SimpleFeatureCollection colEzerai;
    private SimpleFeatureCollection colVanduo;
    private SimpleFeatureCollection colKeliai;
    private SimpleFeatureCollection colKalnai;
    private SimpleFeatureCollection colMiestai;

    public T3CalculationsAction(MainFrame mainFrame) {
        utils = KarGISUtils.getInstance();
        this.mainFrame = mainFrame;
        settingsFrame = new T3SettingsFrame();
        settingsFrame.addWindowListener(new WindowListener() {
            @Override
            public void windowClosed(WindowEvent e) {
                KarGISUtils.getInstance().output(request.toString());

                if (request == null) {
                    return;
                }

                loadAdditionalLayers();
                Thread workerThread = new Thread(new WorkerT3());
                workerThread.start();
            }

            @Override public void windowOpened(WindowEvent e) {}
            @Override public void windowClosing(WindowEvent e) {}
            @Override public void windowIconified(WindowEvent e) {}
            @Override public void windowDeiconified(WindowEvent e) {}
            @Override public void windowActivated(WindowEvent e) {}
            @Override public void windowDeactivated(WindowEvent e) {}
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        settingsFrame.setVisible(true);
        if (request == null) {
            request = new T3Request();
            settingsFrame.setRequest(request);
        }
    }

    private boolean loadAdditionalLayers() {
        if (fsVirsukalnes != null) {
            System.out.print("T3: required shapefiles already loaded");
            return true;
        }

        try {
            fsPlotai = utils.loadNewFS(new File(RES_PLOTAI_URI));
            fsGyvenvietes = utils.loadNewFS(new File(RES_GYVENVIETES_URI));
            fsKeliai = utils.loadNewFS(new File(RES_KELIAI_URI));
            fsHidroL = utils.loadNewFS(new File(RES_HIDRO_L_URI));
            fsVirsukalnes = utils.loadNewFS(new File(RES_VIRSUKALNES_URI));
            KarGISUtils.getInstance().output("T3: required shapefiles loaded: \n" +
                    fsPlotai.toString() + "\n" +
                    fsHidroL.toString() + "\n" +
                    fsKeliai.toString() + "\n" +
                    fsGyvenvietes.toString() + "\n" +
                    fsVirsukalnes.toString() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private void selectNeededCollections() throws CQLException, IOException {
        KarGISFeatureFactory.getInstance().changeCRSIfNeeded(fsKeliai.getSchema().getCoordinateReferenceSystem());
        Stopwatch timer = new Stopwatch();
        KarGISUtils.getInstance().output(timer.getTimeString() + ": T3 started!");
        timer.start();

        // 1 - bufferis apie kelius
        colKeliai = fsKeliai.getFeatures(getKeliaiFilter(request.getKeliai()));
        colKeliai = utils.bufferLayer(colKeliai, request.getKeliuBuffer());
        Geometry keliaiUnion = utils.uniteIntoOneGeometry(colKeliai);
        KarGISUtils.getInstance().output(timer.getTimeString() + ": colKeliai:" + (colKeliai != null ? colKeliai.size() : "null"));
        if (colKeliai == null) {
            mainFrame.addLayer(utils.createLayer(colKeliai));
        }

//        2 - kitų sluoksnių apdorojimas
        if (colUpes == null) {
            colUpes = fsHidroL.getFeatures(CQL.toFilter("PLOTIS >= " + request.getHidroPlotis()));
            colUpes = utils.intersects(keliaiUnion, colUpes);
            colUpes = utils.bufferLayer(colUpes, request.getHidroPlotis());
            KarGISUtils.getInstance().output(timer.getTimeString() + ": colUpes:" + (colUpes != null ? colUpes.size() : "null"));
        }
        if (colEzerai == null) {
            colEzerai = fsPlotai.getFeatures(CQL.toFilter("GKODAS = 'hd3'"));
            colEzerai = utils.intersects(keliaiUnion, colEzerai);
            KarGISUtils.getInstance().output(timer.getTimeString() + ": colEzerai:" + (colEzerai != null ? colEzerai.size() : "null"));
        }
        if (colUpes != null && colEzerai != null) {
            colVanduo = utils.union(colUpes, colEzerai);
        } else {
            if (colUpes != null) {
                colVanduo = colUpes;
            } else if (colEzerai != null) {
                colVanduo = colEzerai;
            } else {
                colVanduo = null;
            }
        }
        KarGISUtils.getInstance().output(timer.getTimeString() + ": colVanduo:" + (colVanduo != null ? colVanduo.size() : "null"));
        colUpes = null;
        colEzerai = null;
        
        if (colKalnai == null) {
            colKalnai = utils.intersects(keliaiUnion, fsVirsukalnes.getFeatures());
            colKalnai = utils.bufferLayer(colKalnai, request.getKalnaiBuffer());
            KarGISUtils.getInstance().output(timer.getTimeString() + ": colKalnai:" + (colKalnai != null ? colKalnai.size() : "null"));
        }

        if (colMiestai == null) {
            colMiestai = fsGyvenvietes.getFeatures(CQL.toFilter("GKODAS = 'uas51'")); // Savivaldybes, ne miestai
            colMiestai = utils.intersects(keliaiUnion, colMiestai);
            colMiestai = utils.bufferLayer(colMiestai, request.getCityBuffer());
            KarGISUtils.getInstance().output(timer.getTimeString() + ": colMiestai:" + (colMiestai != null ? colMiestai.size() : "null"));
        }

        Geometry pakeles = utils.difference(keliaiUnion, utils.uniteIntoOneGeometry(colMiestai));
        colMiestai = null;
        pakeles = utils.difference(pakeles, utils.uniteIntoOneGeometry(colKalnai));
        colKalnai = null;
        pakeles = utils.difference(pakeles, utils.uniteIntoOneGeometry(colVanduo));
        colVanduo = null;

        mainFrame.addLayer(utils.createLayer(pakeles));

        ReferencedEnvelope gridBounds = new ReferencedEnvelope(pakeles.getEnvelopeInternal(), colKeliai.getSchema().getCoordinateReferenceSystem());
        clearFirstHalf();
        KarGISUtils.getInstance().output(timer.getTimeString() + ": Creating grid...");
        SimpleFeatureSource grid = Grids.createSquareGrid(gridBounds, Math.sqrt(request.getPlotas()));
//        mainFrame.addLayer(utils.createLayer(grid.getFeatures()));

        KarGISUtils.getInstance().output(timer.getTimeString() + ": Creating intersecting grid...");
        SimpleFeatureCollection intersectingGrid = utils.intersects(pakeles, grid.getFeatures());
//        mainFrame.addLayer(utils.createLayer(intGrid));
        gridBounds = null;
        grid = null;

        KarGISUtils.getInstance().output(timer.getTimeString() + ": Creating intersected grid...");
        SimpleFeatureCollection intersectedGrid = utils.intersect(pakeles, intersectingGrid);
//        mainFrame.addLayer(utils.createLayer(intersectedGrid));
        intersectingGrid = null;

        SimpleFeatureCollection finalTerritory = intersectedGrid.subCollection(CQL.toFilter("area >= " + request.getPlotas()));
        intersectedGrid = null;
        KarGISUtils.getInstance().output(timer.getTimeString() + ": saving file - location requested");
        utils.createNewFS(finalTerritory);
        mainFrame.addLayer(utils.createLayer(finalTerritory));

        timer.stop();
        KarGISUtils.getInstance().output(this, timer.getTimeString() + ": Done!");
    }

    private Filter getKeliaiFilter(List<String> keliai) throws CQLException {
        StringBuilder sb = new StringBuilder();
        for (String kelias : keliai) {
            if (sb.length() != 0) {
                sb.append(" OR ");
            }
            sb.append("NUMERIS LIKE '" + kelias + "'");
        }
        return CQL.toFilter(sb.toString());
    }

    private void clearFirstHalf() {
        colEzerai = null;
        colMiestai = null;
        colUpes = null;
        colVanduo = null;
        colKalnai = null;
        colKeliai = null;
    }

    private class WorkerT3 implements Runnable {
        @Override
        public void run() {
            try {
                selectNeededCollections();
            } catch (CQLException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}
