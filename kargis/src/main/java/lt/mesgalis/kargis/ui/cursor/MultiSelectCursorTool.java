package lt.mesgalis.kargis.ui.cursor;

import lt.mesgalis.kargis.ui.MainFrame;
import org.geotools.data.FeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.geometry.Envelope2D;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.swing.MapPane;
import org.geotools.swing.event.MapMouseEvent;
import org.geotools.swing.tool.CursorTool;
import org.opengis.feature.Feature;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.identity.FeatureId;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.HashSet;

/**
 * Features multi-selection tool
 *
 * Created by Karolis Jocevičius
 */
public class MultiSelectCursorTool extends CursorTool {
    private static final long serialVersionUID = 694577480554531671L;

    private final FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
    private final MainFrame mainFrame;
    private final FeatureSource featureSource;
    private final Point startPosDevice;
    private final Point2D startPosWorld;
    private MapPane mapPane;
    private boolean dragged;

    public MultiSelectCursorTool(MainFrame mainFrame) {
        this.mapPane = mainFrame.getMapPane();
        this.mainFrame = mainFrame;
        featureSource = mainFrame.getSelectedLayer().getFeatureSource();
        mainFrame.setSelectedIds(new HashSet<FeatureId>());
        mainFrame.setGeometry(featureSource);
        mainFrame.displaySelectedFeatures();

        startPosDevice = new Point();
        startPosWorld = new DirectPosition2D();
        dragged = false;
    }

    @Override
    public void onMouseClicked(MapMouseEvent ev) {
        selectFeatures(ev);
    }

    private void selectFeatures(MapMouseEvent ev) {

        /*
         * Construct a 5x5 pixel rectangle centred on the mouse click position
         */
        Point screenPos = ev.getPoint();
        Rectangle screenRect = new Rectangle(screenPos.x - 2, screenPos.y - 2, 5, 5);

        /*
         * Transform the screen rectangle into bounding box in the coordinate
         * reference system of our map context. Note: we are using a naive method
         * here but GeoTools also offers other, more accurate methods.
         */
        AffineTransform screenToWorld = getMapPane().getScreenToWorldTransform();
        Rectangle2D worldRect = screenToWorld.createTransformedShape(screenRect).getBounds2D();
        ReferencedEnvelope bbox = new ReferencedEnvelope(
                worldRect,
                getMapPane().getMapContent().getCoordinateReferenceSystem());

        /*
         * Create a Filter to select features that intersect with
         * the bounding box
         */
        Filter filter = ff.intersects(ff.property(mainFrame.getGeometryAttributeName()), ff.literal(bbox));

        select(featureSource, filter);
    }

    /**
     * Use the filter to identify the selected features
     */
    private void select(FeatureSource featureSource, Filter filter) {

        try {
            FeatureCollection selectedFeatures = featureSource.getFeatures(filter);

            FeatureIterator iter = selectedFeatures.features();
            try {
                while (iter.hasNext()) {
                    Feature feature = iter.next();
                    mainFrame.getSelectedIds().add(feature.getIdentifier());

                    System.out.println("   " + feature.getIdentifier());
                }

            } finally {
                iter.close();
            }

            if (mainFrame.getSelectedIds().isEmpty()) {
                System.out.println("   no feature selected");
            }

            mainFrame.displaySelectedFeatures(mainFrame.getSelectedIds());
            mainFrame.getQueryFrame().showSelection(featureSource, mainFrame.getSelectedIds());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Records the map position of the mouse event in case this
     * button press is the beginning of a mouse drag
     *
     * @param ev the mouse event
     */
    @Override
    public void onMousePressed(MapMouseEvent ev) {
        startPosDevice.setLocation(ev.getPoint());
        startPosWorld.setLocation(ev.getWorldPos());
    }

    /**
     * Records that the mouse is being dragged
     *
     * @param ev the mouse event
     */
    @Override
    public void onMouseDragged(MapMouseEvent ev) {
        dragged = true;
    }

    /**
     * If the mouse was dragged, determines the bounds of the
     * box that the user defined and marks as selected
     *
     * @param ev the mouse event
     */
    @Override
    public void onMouseReleased(MapMouseEvent ev) {
        if (dragged && !ev.getPoint().equals(startPosDevice)) {

            Envelope2D env = new Envelope2D();
            env.setFrameFromDiagonal(startPosWorld, ev.getWorldPos());

            Rectangle2D worldRect = env.getBounds2D();
            ReferencedEnvelope bbox = new ReferencedEnvelope(
                    worldRect,
                    getMapPane().getMapContent().getCoordinateReferenceSystem());

            dragged = false;
            Filter filter = ff.intersects(ff.property(mainFrame.getGeometryAttributeName()), ff.literal(bbox));
            select(featureSource, filter);
        }
    }

    /**
     * Returns true to indicate that this tool draws a box
     * on the map display when the mouse is being dragged to
     * show the zoom-in area
     */
    @Override
    public boolean drawDragBox() {
        return true;
    }

    @Override
    public MapPane getMapPane() {
        return mapPane;
    }

    public void setMapPane(MapPane mapPane) {
        this.mapPane = mapPane;
    }
}
