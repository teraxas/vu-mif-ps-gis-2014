package lt.mesgalis.kargis.ui;

import com.sun.deploy.util.StringUtils;
import lt.mesgalis.kargis.KarGISUtils;
import lt.mesgalis.kargis.model.T3Request;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Karolis Jocevičius
 */
public class T3SettingsFrame extends JFrame {

    private JLabel labelRoads = new JLabel("Keliai (atskirti tarpais)");
    private JLabel labelRoadsBuffer = new JLabel("Buferis aplink kelius");
    private JLabel labelCityBuffer = new JLabel("Buferis aplink miestus");
    private JLabel labelMaxUpiuPlotis = new JLabel("Maksimalus upių plotis (m)");
    private JLabel labelMinArea = new JLabel("Minimalus plotas");
    private JLabel labelKalnaiBuffer = new JLabel("Buferis aplink viršukalnes");

    private JTextField fieldRoads;
    private JTextField fieldRoadsBuffer;
    private JTextField fieldCityBuffer;
    private JTextField fieldMaxUpiuPlotis;
    private JTextField fieldMinArea;
    private JTextField fieldKalnaiBuffer;

    private JButton butDone = new JButton("Patvirtinti");
    private JButton butCancel = new JButton("Atšaukti");

    private JPanel contentPane;

    private T3Request request;

    public T3SettingsFrame() {
        initButs();
        initUI();
    }

    private void initButs() {
        butDone.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    request.setPlotas(Double.parseDouble(fieldMinArea.getText()));
                    request.setCityBuffer(Double.parseDouble(fieldCityBuffer.getText()));
                    request.setHidroPlotis(Double.parseDouble(fieldMaxUpiuPlotis.getText()));
                    request.setKeliuBuffer(Double.parseDouble(fieldRoadsBuffer.getText()));
                    request.setKalnaiBuffer(Double.parseDouble(fieldKalnaiBuffer.getText()));
                    request.getKeliai().clear();
                    for (String str : StringUtils.splitString(fieldRoads.getText(), " ")) {
                        request.getKeliai().add(str);
                    }
                } catch (Exception ex) {
                    KarGISUtils.getInstance().displayMessage("Netinkamas formatas! \n" + ex.getMessage());
                }
                request.setConfirm(true);
                dispose();
            }
        });
        butCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                request.setConfirm(false);
            }
        });
    }

    private void initUI() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 300, 300);
        fieldRoads = new JTextField();
        fieldRoadsBuffer = new JTextField();
        fieldCityBuffer = new JTextField();
        fieldMaxUpiuPlotis = new JTextField();
        fieldMinArea = new JTextField();
        fieldKalnaiBuffer = new JTextField();

        contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(0, 2));

        contentPane.add(labelRoads);
        contentPane.add(fieldRoads);

        contentPane.add(labelRoadsBuffer);
        contentPane.add(fieldRoadsBuffer);

        contentPane.add(labelCityBuffer);
        contentPane.add(fieldCityBuffer);

        contentPane.add(labelMaxUpiuPlotis);
        contentPane.add(fieldMaxUpiuPlotis);

        contentPane.add(labelMinArea);
        contentPane.add(fieldMinArea);

        contentPane.add(labelKalnaiBuffer);
        contentPane.add(fieldKalnaiBuffer);

        contentPane.add(butDone);
        contentPane.add(butCancel);

        add(contentPane);
    }

    public T3Request getRequest() {
        return request;
    }

    public void setRequest(T3Request request) {
        this.request = request;
        fieldKalnaiBuffer.setText(String.valueOf(request.getKalnaiBuffer()));
        fieldMaxUpiuPlotis.setText(String.valueOf(request.getHidroPlotis()));
        fieldRoads.setText(String.valueOf(request.getKeliai()));
        fieldMinArea.setText(String.valueOf(request.getPlotas()));
        fieldCityBuffer.setText(String.valueOf(request.getCityBuffer()));
        fieldRoadsBuffer.setText(String.valueOf(request.getKeliuBuffer()));
    }
}
