package lt.mesgalis.kargis.ui.actions;

import com.vividsolutions.jts.util.Stopwatch;
import lt.mesgalis.kargis.KarGISUtils;
import lt.mesgalis.kargis.model.T2Result;
import lt.mesgalis.kargis.model.T2ResultRatesModel;
import lt.mesgalis.kargis.model.T2ResultTableModel;
import lt.mesgalis.kargis.ui.GeomType;
import lt.mesgalis.kargis.ui.MainFrame;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.filter.text.cql2.CQLException;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.geometry.BoundingBox;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karolis Jocevičius
 */
public class T2CalculationsAction extends AbstractAction {

    private static final String RES_PLOTAI_PATH = "resource/T2res/PLOTAI_sel.shp";
    private static final String RES_HIDRO_L_PATH = "resource/T2res/HIDRO_L_sel.shp";
    private static final String RES_KELIAI_PATH = "resource/T2res/KELIAI_sel.shp";
    private static final String RES_PASTAT_P_PATH = "resource/T2res/PASTAT_P_sel.shp";

    private final FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
    private final KarGISUtils utils;

    private final MainFrame mainFrame;

    private SimpleFeatureSource fsPlotai;
    private SimpleFeatureSource fsHidroL;
    private SimpleFeatureSource fsKeliai;
    private SimpleFeatureSource fsPastatP;

    public T2CalculationsAction(MainFrame frame) {
        this.mainFrame = frame;
        utils = KarGISUtils.getInstance();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (mainFrame.getSelectedIds() == null || mainFrame.getSelectedIds().isEmpty()) {
            utils.displayMessage("Nothing selected!");
            return;
        }
        if (!mainFrame.getGeometryType().equals(GeomType.POLYGON)) {
            utils.displayMessage("A polygon must be selected!");
            return;
        }
        if (!loadAdditionalLayers()) {
            utils.displayMessage("Could not load required layers from 'resource' folder");
            return;
        }

        SimpleFeatureSource fsMain = (SimpleFeatureSource) mainFrame.getSelectedLayer().getFeatureSource();

        SimpleFeatureCollection selectedFeatures;
        try {
            selectedFeatures = fsMain.getFeatures(ff.id(mainFrame.getSelectedIds()));
        } catch (IOException e1) {
            e1.printStackTrace();
            utils.displayMessage("T2: Error while selecting features");
            return;
        }

        new T2ExecutionThread(selectedFeatures);
//        run(selectedFeatures);
    }

    private T2Result calculate(SimpleFeature feature) throws IOException, CQLException {
        Stopwatch timer = new Stopwatch();
        KarGISUtils.getInstance().output(timer.getTimeString() + ": T3 started!");
        KarGISUtils.getInstance().output("Calculating T2 for feature: " + feature.getIdentifier());

        T2Result result = new T2Result();

        BoundingBox bbox = feature.getBounds();
        Filter mainBoxFilter = ff.intersects(ff.property(mainFrame.getGeometryAttributeName()), ff.literal(bbox));
        Filter mainFilter = ff.intersects(ff.property(mainFrame.getGeometryAttributeName()), ff.literal(feature.getDefaultGeometryProperty().getValue()));
        Filter andFilter = ff.and(mainBoxFilter, mainFilter);

        SimpleFeatureCollection fsPlotaiShaped = fsPlotai.getFeatures(mainFilter);
        SimpleFeatureCollection fsPastataiShaped = fsPastatP.getFeatures(mainFilter);

        result.setPlotas((Double) feature.getAttribute("PLOT"));

//        MultiPolygon poly = (MultiPolygon) feature.getDefaultGeometry();
//        result.setPlotas(poly.getArea());

        KarGISUtils.getInstance().output(timer.getTimeString() + ": Calculating KELIAI...");
        SimpleFeatureCollection fsKeliaiFeatures = fsKeliai.getFeatures(mainFilter);
        SimpleFeatureCollection fsKeliaiIntersected = utils.intersect(feature, fsKeliaiFeatures);
        result.setKeliuIlgis(utils.getSum(fsKeliaiIntersected, "length"));

        KarGISUtils.getInstance().output(timer.getTimeString() + ": Calculating HIDRO_L - 1 ...");
        SimpleFeatureCollection fsHidroUpes = fsHidroL.getFeatures(ff.and(mainFilter, CQL.toFilter("TIPAS = '1'"))); //upes
        SimpleFeatureCollection fsHidroIntersected = utils.intersect(feature, fsHidroUpes);
        result.setUpiuIlgis(utils.getSum(fsHidroIntersected, "length"));

        KarGISUtils.getInstance().output(timer.getTimeString() + ": Calculating PLOTAI - hd...");
        SimpleFeatureCollection fsPlotaiHD = fsPlotaiShaped.subCollection(CQL.toFilter("GKODAS LIKE 'hd%'"));
        SimpleFeatureCollection fsPlotaiHDIntersected = utils.intersect(feature, fsPlotaiHD);
        result.setPlotasHD(utils.getSum(fsPlotaiHDIntersected, "area"));

        KarGISUtils.getInstance().output(timer.getTimeString() + ": Calculating PLOTAI - ms0...");
        SimpleFeatureCollection fsPlotaiMS0 = fsPlotaiShaped.subCollection(CQL.toFilter("GKODAS = 'ms0'"));
        SimpleFeatureCollection fsPlotaiMS0Intersected = utils.intersect(feature, fsPlotaiMS0);
        result.setPlotasMS0(utils.getSum(fsPlotaiMS0Intersected, "area"));

        KarGISUtils.getInstance().output(timer.getTimeString() + ": Calculating PLOTAI - ms4...");
        SimpleFeatureCollection fsPlotaiMS4 = fsPlotaiShaped.subCollection(CQL.toFilter("GKODAS = 'ms4'"));
        SimpleFeatureCollection fsPlotaiMS4Intersected = utils.intersect(feature, fsPlotaiMS4);
        result.setPlotasMS4(utils.getSum(fsPlotaiMS4Intersected, "area"));

        KarGISUtils.getInstance().output(timer.getTimeString() + ": Calculating PLOTAI - pu0...");
        SimpleFeatureCollection fsPlotaiPU0 = fsPlotaiShaped.subCollection(CQL.toFilter("GKODAS = 'pu0'"));
        SimpleFeatureCollection fsPlotaiPU0Intersected = utils.intersect(feature, fsPlotaiPU0);
        result.setPlotasPU0(utils.getSum(fsPlotaiPU0Intersected, "area"));

        KarGISUtils.getInstance().output(timer.getTimeString() + ": Calculating PASTAT_P");
        SimpleFeatureCollection fsPastataiIntersected = utils.intersect(feature, fsPastataiShaped);
        result.setPastatuPlotas(utils.getSum(fsPastataiIntersected, "area"));

        KarGISUtils.getInstance().output(timer.getTimeString() + ": Calculating PASTAT_P - hd...");
        SimpleFeatureCollection pastataiHD = utils.intersect(fsPlotaiHDIntersected, fsPastataiIntersected);
        result.setPastataiHD(utils.getSum(pastataiHD, "area"));

        KarGISUtils.getInstance().output(timer.getTimeString() + ": Calculating PASTAT_P - ms0...");
        SimpleFeatureCollection pastataiMS0 = utils.intersect(fsPlotaiMS0Intersected, fsPastataiIntersected);
        result.setPastataiMS0(utils.getSum(pastataiMS0, "area"));

        KarGISUtils.getInstance().output(timer.getTimeString() + ": Calculating PASTAT_P - ms4...");
        SimpleFeatureCollection pastataiMS4 = utils.intersect(fsPlotaiMS4Intersected, fsPastataiIntersected);
        result.setPastataiMS4(utils.getSum(pastataiMS4, "area"));

        KarGISUtils.getInstance().output(timer.getTimeString() + ": Calculating PASTAT_P - pu0...");
        SimpleFeatureCollection pastataiPU0 = utils.intersect(fsPlotaiPU0Intersected, fsPastataiIntersected);
        result.setPastataiPU0(utils.getSum(pastataiPU0, "area"));

        timer.stop();
        KarGISUtils.getInstance().output(this, timer.getTimeString() + ": Done!");
        return result;
    }

    private boolean loadAdditionalLayers() {
        if (fsHidroL != null && fsKeliai != null && fsPastatP != null && fsPlotai != null) {
            KarGISUtils.getInstance().output("T2: required shapefiles already loaded");
            return true;
        }

        try {
            fsPlotai = utils.loadNewFS(new File(RES_PLOTAI_PATH));
            fsPastatP = utils.loadNewFS(new File(RES_PASTAT_P_PATH));
            fsKeliai = utils.loadNewFS(new File(RES_KELIAI_PATH));
            fsHidroL = utils.loadNewFS(new File(RES_HIDRO_L_PATH));
            KarGISUtils.getInstance().output("T2: required shapefiles loaded: \n" +
                    fsPlotai.toString() + "\n" +
                    fsHidroL.toString() + "\n" +
                    fsKeliai.toString() + "\n" +
                    fsPastatP.toString());
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private void run(SimpleFeatureCollection selectedFeatures) {
        ArrayList<T2Result> results = new ArrayList<T2Result>();

        SimpleFeatureIterator iter = selectedFeatures.features();
        while (iter.hasNext()) {
            SimpleFeature feature = iter.next();
            try {
                results.add(calculate(feature));
            } catch (Exception e1) {
                e1.printStackTrace();
                utils.displayMessage("T2: Filtering error");
                return;
            }
        }

        utils.displayTable(new T2ResultTableModel(results, T2Result.COLUMN_NAMES), "T2 results");
        utils.displayTable(new T2ResultTableModel(convertToRates(results), T2ResultRatesModel.COLUMN_NAMES), "T2 rates");
    }

    private List<T2ResultRatesModel> convertToRates(ArrayList<T2Result> results) {
        List<T2ResultRatesModel> result = new ArrayList<T2ResultRatesModel>();
        for (T2Result res : results) {
            result.add(new T2ResultRatesModel(res));
        }
        return result;
    }

    private class T2ExecutionThread extends Thread {

        private final SimpleFeatureCollection selectedFeatures;

        public T2ExecutionThread(SimpleFeatureCollection selectedFeatures) {
            this.selectedFeatures = selectedFeatures;
            start();
        }

        @Override
        public void run() {
            ArrayList<T2Result> results = new ArrayList<T2Result>();

            SimpleFeatureIterator iter = selectedFeatures.features();
            while (iter.hasNext()) {
                SimpleFeature feature = iter.next();
                try {
                    results.add(calculate(feature));
                } catch (Exception e1) {
                    e1.printStackTrace();
                    utils.displayMessage("T2: Filtering error");
                    return;
                }
            }

            utils.displayTable(new T2ResultTableModel(results, T2Result.COLUMN_NAMES), "T2 results");
            utils.displayTable(new T2ResultTableModel(convertToRates(results), T2ResultRatesModel.COLUMN_NAMES), "T2 rates");
        }


    }


}
