package lt.mesgalis.kargis;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

/**
 * Simplifies creation of SimpleFeature objects.
 *
 * Created by Karolis Jocevičius
 */
public class KarGISFeatureFactory {

    private static volatile KarGISFeatureFactory instance;

    private SimpleFeatureBuilder pointFeatureBuilder;
    private SimpleFeatureBuilder lineFeatureBuilder;
    private SimpleFeatureBuilder polygonFeatureBuilder;

    private KarGISFeatureFactory() {}

    /**
     * Get instance of KarGISFeatureFactory
     *
     * @return
     */
    public static KarGISFeatureFactory getInstance() {
        if (instance == null) {
            synchronized (KarGISFeatureFactory.class) {
                instance = new KarGISFeatureFactory();
            }
        }
        return instance;
    }

    /**
     * Sets base CRS and recreates base FeatureBuilders
     *
     * @param crs
     */
    public void changeCRS(CoordinateReferenceSystem crs) {
        pointFeatureBuilder = new SimpleFeatureBuilder(createFeatureTypePoint(crs));
        lineFeatureBuilder = new SimpleFeatureBuilder(createFeatureTypeLine(crs));
        polygonFeatureBuilder = new SimpleFeatureBuilder(createFeatureTypePolygon(crs));
    }

    /**
     * Sets base CRS and recreates base FeatureBuilders if needed
     *
     * @param crs
     */
    public void changeCRSIfNeeded(CoordinateReferenceSystem crs) {
        if (pointFeatureBuilder == null || lineFeatureBuilder == null || polygonFeatureBuilder == null) {
            changeCRS(crs);
        }
    }

    /**
     * Creates basic feature with recalculated length / area fields
     *
     * @param geom
     * @return
     */
    public SimpleFeature createFeature(Geometry geom) {
        return createFeature(geom, "NULL", 0);
    }

    /**
     * Creates basic feature with recalculated length / area fields and predefined name, number fields
     *
     * @param geom   geometry
     * @param name   name field value
     * @param number number field value
     * @return
     */
    @SuppressWarnings("SameParameterValue")
    SimpleFeature createFeature(Geometry geom, String name, int number) {
        SimpleFeature newFeature = null;
        if (geom instanceof Polygon) {
            polygonFeatureBuilder.add(geom);
            polygonFeatureBuilder.add(name);
            polygonFeatureBuilder.add(0);
            polygonFeatureBuilder.add(geom.getArea());
            newFeature = polygonFeatureBuilder.buildFeature(null);
        } else if (geom instanceof LineString) {
            lineFeatureBuilder.add(geom);
            lineFeatureBuilder.add(name);
            lineFeatureBuilder.add(0);
            lineFeatureBuilder.add(geom.getLength());
            newFeature = lineFeatureBuilder.buildFeature(null);
        } else if (geom instanceof Point) {
            pointFeatureBuilder.add(geom);
            pointFeatureBuilder.add(name);
            pointFeatureBuilder.add(0);
            newFeature = pointFeatureBuilder.buildFeature(null);
        } else {
            throw new IllegalStateException("Unknown geometry type: " + geom.getGeometryType());
        }
        return newFeature;
    }

    SimpleFeatureType createFeatureTypePoint(CoordinateReferenceSystem crs) {

        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setName("Location");
        builder.setCRS(crs); // <- Coordinate reference system

        // add attributes in order
        builder.add("the_geom", Point.class);
        builder.length(15).add("Name", String.class); // <- 15 chars width for name field
        builder.add("number", Integer.class);

        // build the type
        return builder.buildFeatureType();
    }

    SimpleFeatureType createFeatureTypeLine(CoordinateReferenceSystem crs) {

        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setName("Location");
        builder.setCRS(crs); // <- Coordinate reference system

        // add attributes in order
        builder.add("the_geom", LineString.class);
        builder.length(15).add("Name", String.class); // <- 15 chars width for name field
        builder.add("number", Integer.class);
        builder.add("length", Double.class);

        // build the type
        return builder.buildFeatureType();
    }

    SimpleFeatureType createFeatureTypePolygon(CoordinateReferenceSystem crs) {

        SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
        builder.setName("Location");
        builder.setCRS(crs); // <- Coordinate reference system

        // add attributes in order
        builder.add("the_geom", Polygon.class);
        builder.length(15).add("Name", String.class); // <- 15 chars width for name field
        builder.add("number", Integer.class);
        builder.add("area", Double.class);

        // build the type
        return builder.buildFeatureType();
    }

    private void checkBaseBuilders() {
        if (polygonFeatureBuilder == null || pointFeatureBuilder == null || lineFeatureBuilder == null) {
            throw new IllegalStateException("Factory CRS not set. Use changeCRS(CoordinateReferenceSystem crs)");
        }
    }

}
