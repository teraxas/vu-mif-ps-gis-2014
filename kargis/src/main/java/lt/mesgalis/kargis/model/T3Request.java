package lt.mesgalis.kargis.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karolis Jocevičius
 *
 * Task 3 request model
 */
public class T3Request {

    private double cityBuffer = 1000; // m
    private double hidroPlotis = 10; // m
    private double plotas = 1000000; // m^3
    private double keliuBuffer = 10000; // m
    private double kalnaiBuffer = 100; // m
    private List<String> keliai = new ArrayList<String>();

    private boolean confirm = false;

    @Override
    public String toString() {
        return super.toString() + ": cityBuffer=" + cityBuffer + " hidroPlotis=" + hidroPlotis + " plotas=" + plotas + " keliuBuffer=" + keliuBuffer + "\n" +
                "keliai={" + keliai + "}";
    }

    public double getCityBuffer() {
        return cityBuffer;
    }

    public void setCityBuffer(double cityBuffer) {
        this.cityBuffer = cityBuffer;
    }

    public double getHidroPlotis() {
        return hidroPlotis;
    }

    public void setHidroPlotis(double hidroPlotis) {
        this.hidroPlotis = hidroPlotis;
    }

    public double getPlotas() {
        return plotas;
    }

    public void setPlotas(double plotas) {
        this.plotas = plotas;
    }

    public double getKeliuBuffer() {
        return keliuBuffer;
    }

    public void setKeliuBuffer(double keliuBuffer) {
        this.keliuBuffer = keliuBuffer;
    }

    public List<String> getKeliai() {
        return keliai;
    }

    public void setKeliai(List<String> keliai) {
        this.keliai = keliai;
    }

    public double getKalnaiBuffer() {
        return kalnaiBuffer;
    }

    public void setKalnaiBuffer(double kalnaiBuffer) {
        this.kalnaiBuffer = kalnaiBuffer;
    }

    public boolean isConfirm() {
        return confirm;
    }

    public void setConfirm(boolean confirm) {
        this.confirm = confirm;
    }
}
