package lt.mesgalis.kargis.model;

/**
 * Created by Karolis Jocevičius
 */
interface IResultModel {

    double getValueAtPos(int pos);

    String[] getColumnNames();
}
