package lt.mesgalis.kargis.model;

/**
 * Created by Karolis Jocevičius
 */
public class T2Result implements IResultModel {

    public static final String[] COLUMN_NAMES = new String[]{
            "plotas",
            "upiuIlgis", "keliuIlgis",
            "plotasHD", "plotasMS0", "plotasMS4", "plotasPU0",
            "pastatuPlotas", "pastataiHD", "pastataiMS0", "pastataiMS4", "pastataiPU0"
    };

    private double plotas;

    private double upiuIlgis;
    private double keliuIlgis;

    private double plotasHD;
    private double plotasMS0;
    private double plotasMS4;
    private double plotasPU0;

    private double pastatuPlotas;
    private double pastataiHD;
    private double pastataiMS0;
    private double pastataiMS4;
    private double pastataiPU0;

    public double getValueAtPos(int pos) {
        switch (pos) {
            case 0:
                return plotas;
            case 1:
                return upiuIlgis;
            case 2:
                return keliuIlgis;
            case 3:
                return plotasHD;
            case 4:
                return plotasMS0;
            case 5:
                return plotasMS4;
            case 6:
                return plotasPU0;
            case 7:
                return pastatuPlotas;
            case 8:
                return pastataiHD;
            case 9:
                return pastataiMS0;
            case 10:
                return pastataiMS4;
            case 11:
                return pastataiPU0;
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public String[] getColumnNames() {
        return COLUMN_NAMES;
    }

    public double getPlotas() {
        return plotas;
    }

    public void setPlotas(double plotas) {
        this.plotas = plotas;
    }

    public double getUpiuIlgis() {
        return upiuIlgis;
    }

    public void setUpiuIlgis(double upiuIlgis) {
        this.upiuIlgis = upiuIlgis;
    }

    public double getKeliuIlgis() {
        return keliuIlgis;
    }

    public void setKeliuIlgis(double keliuIlgis) {
        this.keliuIlgis = keliuIlgis;
    }

    public double getPlotasHD() {
        return plotasHD;
    }

    public void setPlotasHD(double plotasHD) {
        this.plotasHD = plotasHD;
    }

    public double getPlotasMS0() {
        return plotasMS0;
    }

    public void setPlotasMS0(double plotasMS0) {
        this.plotasMS0 = plotasMS0;
    }

    public double getPlotasMS4() {
        return plotasMS4;
    }

    public void setPlotasMS4(double plotasMS4) {
        this.plotasMS4 = plotasMS4;
    }

    public double getPlotasPU0() {
        return plotasPU0;
    }

    public void setPlotasPU0(double plotasPU0) {
        this.plotasPU0 = plotasPU0;
    }

    public double getPastatuPlotas() {
        return pastatuPlotas;
    }

    public void setPastatuPlotas(double pastatuPlotas) {
        this.pastatuPlotas = pastatuPlotas;
    }

    public double getPastataiHD() {
        return pastataiHD;
    }

    public void setPastataiHD(double pastataiHD) {
        this.pastataiHD = pastataiHD;
    }

    public double getPastataiMS0() {
        return pastataiMS0;
    }

    public void setPastataiMS0(double pastataiMS0) {
        this.pastataiMS0 = pastataiMS0;
    }

    public double getPastataiMS4() {
        return pastataiMS4;
    }

    public void setPastataiMS4(double pastataiMS4) {
        this.pastataiMS4 = pastataiMS4;
    }

    public double getPastataiPU0() {
        return pastataiPU0;
    }

    public void setPastataiPU0(double pastataiPU0) {
        this.pastataiPU0 = pastataiPU0;
    }
}
