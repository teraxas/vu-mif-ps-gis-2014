package lt.mesgalis.kargis.model;

import javax.swing.table.AbstractTableModel;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Karolis Jocevičius
 */
public class T2ResultTableModel<T extends IResultModel> extends AbstractTableModel {

    private final DecimalFormat format = new DecimalFormat("########.#############");

    private final List<T> results;
    private final String[] columnNames;

    public T2ResultTableModel(List<T> results, String[] columnNames) {
        this.results = results;
        this.columnNames = columnNames;
    }

    @Override
    public int getRowCount() {
        return results.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return format.format(results.get(rowIndex).getValueAtPos(columnIndex));
    }

}
