package lt.mesgalis.kargis.model;

/**
 * Created by Karolis Jocevičius
 */
public class T2ResultRatesModel implements IResultModel {

    public static final String[] COLUMN_NAMES = new String[]{
            "plotas",
            "plotasHD/plotas", "plotasMS0/plotas", "plotasMS4/plotas", "plotasPU0/plotas",
            "pastatuPlotas/plotas", "pastataiHD/plotasHD", "pastataiMS0/plotasMS0", "pastataiMS4/plotasMS4", "pastataiPU0/plotasPU0"
    };

    private final T2Result result;

    public T2ResultRatesModel(T2Result result) {
        this.result = result;
    }

    public double getValueAtPos(int pos) {
        switch (pos) {
            case 0:
                return result.getPlotas();
            case 1:
                return getRate(result.getPlotasHD());
            case 2:
                return getRate(result.getPlotasMS0());
            case 3:
                return getRate(result.getPlotasMS4());
            case 4:
                return getRate(result.getPlotasPU0());
            case 5:
                return getRate(result.getPastatuPlotas());
            case 6:
                return getRate(result.getPastataiHD(), result.getPlotasHD());
            case 7:
                return getRate(result.getPastataiMS0(), result.getPlotasMS0());
            case 8:
                return getRate(result.getPastataiMS4(), result.getPlotasMS0());
            case 9:
                return getRate(result.getPastataiPU0(), result.getPlotasPU0());
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public String[] getColumnNames() {
        return COLUMN_NAMES;
    }

    private double getRate(double val) {
        return val / result.getPlotas();
    }

    private double getRate(double val, double val2) {
        return val / val2;
    }

}
