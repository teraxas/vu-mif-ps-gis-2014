package lt.mesgalis.kargis;

import lt.mesgalis.kargis.ui.MainFrame;
import org.geotools.map.MapContent;

import javax.swing.*;

/**
 * KarGIS
 *
 * @author karolis
 */
class KarGIS {

    private static final boolean TEST = false;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        } catch (InstantiationException e1) {
            e1.printStackTrace();
        } catch (IllegalAccessException e1) {
            e1.printStackTrace();
        } catch (UnsupportedLookAndFeelException e1) {
            e1.printStackTrace();
        }

        KarGISUtils utils = KarGISUtils.getInstance();
        utils.initOutput("- - - - - - K - a - r - G - I - S - - -" +
                "\n    by Karolis Jocevičius" +
                "\n---------------------------------------");

        // Create a map content and add our shapefile to it
        MapContent map = new MapContent();
        map.setTitle("KarGis");

        // Now display the map
        MainFrame frame = new MainFrame(map);
        frame.setVisible(true);
    }
}
