package lt.mesgalis.kargis;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import lt.mesgalis.kargis.model.T2ResultTableModel;
import lt.mesgalis.kargis.ui.OutputFrame;
import org.geotools.data.*;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.feature.visitor.FeatureCalc;
import org.geotools.feature.visitor.SumVisitor;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.styling.SLD;
import org.geotools.styling.Style;
import org.geotools.swing.data.JFileDataStoreChooser;
import org.geotools.util.NullProgressListener;
import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.type.GeometryDescriptor;
import org.opengis.filter.Filter;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * Created by Karolis Jocevičius
 * <p/>
 * Utils singleton
 *
 * Various GIS operations based on Geotools lib and some commonly used tools
 */
public class KarGISUtils {

    private static volatile KarGISUtils instance;

    private OutputFrame outputFrame;

    private KarGISUtils() {}

    public static KarGISUtils getInstance() {
        if (instance == null) {
            synchronized (KarGISUtils.class) {
                instance = new KarGISUtils();
            }
        }
        return instance;
    }

    /**
     * display dialog with message
     * @param msg message to display
     */
    public void displayMessage(String msg) {
        JOptionPane.showMessageDialog(null, msg);
        output(msg);
    }

    public void initOutput(String msg) {
        outputFrame = new OutputFrame();
        output(msg);
    }

    /**
     * Outputs message to Output window
     * @param msg message to display
     */
    public void output(String msg) {
        outputFrame.print(msg);
    }

    /**
     * Outputs message to Output window, adds senders class name to begining
     * @param sender sender object
     * @param msg message to display
     */
    public void output(Object sender, String msg) {
        outputFrame.print(sender, msg);
    }

    /**
     * load SimpleFeatureSource from file
     * @param file
     * @return
     * @throws IOException
     */
    public SimpleFeatureSource loadNewFS(File file) throws IOException {
        // display a data store file chooser dialog for shapefiles
        if (file == null) {
            throw new NullPointerException();
        }

        FileDataStore store = null;
        SimpleFeatureSource featureSource = null;
        store = FileDataStoreFinder.getDataStore(file);
        featureSource = store.getFeatureSource();

        return featureSource;
    }

    /**
     * create new feature source as shp file
     * @param file
     * @return
     * @throws IOException
     */
    public void createNewFS(SimpleFeatureCollection collection) throws IOException {
        File newFile = getNewShapeFile();

        ShapefileDataStoreFactory dataStoreFactory = new ShapefileDataStoreFactory();

        Map<String, Serializable> params = new HashMap<String, Serializable>();
        params.put("url", newFile.toURI().toURL());
        params.put("create spatial index", Boolean.TRUE);

        ShapefileDataStore newDataStore = (ShapefileDataStore) dataStoreFactory.createNewDataStore(params);

        newDataStore.createSchema(collection.getSchema());

        /*
         * Write the features to the shapefile
         */
        Transaction transaction = new DefaultTransaction("create");

        String typeName = newDataStore.getTypeNames()[0];
        SimpleFeatureSource featureSource = newDataStore.getFeatureSource(typeName);

        if (featureSource instanceof SimpleFeatureStore) {
            SimpleFeatureStore featureStore = (SimpleFeatureStore) featureSource;

            featureStore.setTransaction(transaction);
            try {
                featureStore.addFeatures(collection);
                transaction.commit();

            } catch (Exception problem) {
                problem.printStackTrace();
                transaction.rollback();

            } finally {
                transaction.close();
            }
        } else {
            System.out.println(typeName + " does not support read/write access");
        }
    }

    /**
     * Prompt the user for the name and path to use for the output shapefile
     *
     * @return name and path for the shapefile as a new File object
     */
    public File getNewShapeFile() {
        String newPath = "untitled.shp";

        JFileDataStoreChooser chooser = new JFileDataStoreChooser("shp");
        chooser.setDialogTitle("Save shapefile");
        chooser.setSelectedFile(new File(newPath));

        int returnVal = chooser.showSaveDialog(null);

        if (returnVal != JFileDataStoreChooser.APPROVE_OPTION) {
            // the user cancelled the dialog
            return null;
        }

        return chooser.getSelectedFile();
    }

    /**
     * Retrieves geometry attribute name (usually "the_geom")
     * @param featureSource
     * @return
     */
    public String getGeometryAttributeName(SimpleFeatureSource featureSource) {
        GeometryDescriptor geomDesc = featureSource.getSchema().getGeometryDescriptor();
        return geomDesc.getLocalName();
    }

    /**
     * Retrieves geometry attribute name (usually "the_geom")
     * @param featureCollection
     * @return
     */
    public String getGeometryAttributeName(SimpleFeatureCollection featureCollection) {
        GeometryDescriptor geomDesc = featureCollection.getSchema().getGeometryDescriptor();
        return geomDesc.getLocalName();
    }

    /**
     * Use the filter to identify the selected features
     */
    public Set<Feature> select(FeatureSource featureSource, Filter filter) {
        Set<Feature> selected = null;
        try {
            FeatureCollection selectedFeatures = featureSource.getFeatures(filter);
            FeatureIterator iter = selectedFeatures.features();
            if (!iter.hasNext()) {
                return selected;
            }

            selected = new HashSet<Feature>();
            try {
                while (iter.hasNext()) {
                    Feature feature = iter.next();
                    selected.add(feature);
                    System.out.println("   " + feature.getIdentifier());
                }
            } finally {
                iter.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return selected;
        }
        return selected;
    }

    /**
     * checks collection for intersecting features with other collection.
     * DOES NOT INTERSECT
     * @param features collection of features
     * @param featuresIn collection of features
     * @return collection of basic features with reclculated length, area values
     */
    public SimpleFeatureCollection intersects(SimpleFeatureCollection features, SimpleFeatureCollection featuresIn) {
        List<SimpleFeature> newFeatures = new ArrayList<SimpleFeature>();

        SimpleFeatureIterator iterator = features.features();
        while (iterator.hasNext()) {
            SimpleFeature feature = iterator.next();
            Geometry geometry = (Geometry) feature.getDefaultGeometry();

            SimpleFeatureIterator iteratorIn = featuresIn.features();
            while (iteratorIn.hasNext()) {
                SimpleFeature featureIn = iteratorIn.next();
                Geometry geometryIn = (Geometry) featureIn.getDefaultGeometry();

                if (!geometry.intersects(geometryIn)) {
                    continue;
                }

                newFeatures.add(featureIn);
            }
        }
        if (newFeatures.isEmpty()) {
            return null;
        }
        return new ListFeatureCollection(newFeatures.get(0).getFeatureType(), newFeatures);
    }

    /**
     * checks collection for intersecting features with other geometry.
     * DOES NOT INTERSECT
     * @param geometry
     * @param featuresIn collection of features
     * @return collection of basic features with reclculated length, area values
     */
    public SimpleFeatureCollection intersects(Geometry geometry, SimpleFeatureCollection featuresIn) {
        List<SimpleFeature> newFeatures = new ArrayList<SimpleFeature>();

        SimpleFeatureIterator iteratorIn = featuresIn.features();
        while (iteratorIn.hasNext()) {
            SimpleFeature featureIn = iteratorIn.next();
            Geometry geometryIn = (Geometry) featureIn.getDefaultGeometry();

            if (!geometry.intersects(geometryIn)) {
                continue;
            }

            newFeatures.add(featureIn);
        }

        if (newFeatures.isEmpty()) {
            return null;
        }
        return new ListFeatureCollection(newFeatures.get(0).getFeatureType(), newFeatures);
    }

    /**
     * intersects two collections of features
     * @param features collection of features
     * @param featuresIn collection of features
     * @return collection of basic features with reclculated length, area values
     */
    public SimpleFeatureCollection intersect(SimpleFeatureCollection features, SimpleFeatureCollection featuresIn) {
        List<SimpleFeature> newFeatures = new ArrayList<SimpleFeature>();

        SimpleFeatureIterator iterator = features.features();
        while (iterator.hasNext()) {
            SimpleFeature feature = iterator.next();
            Geometry geometry = (Geometry) feature.getDefaultGeometry();

            SimpleFeatureIterator iteratorIn = featuresIn.features();
            while (iteratorIn.hasNext()) {
                SimpleFeature featureIn = iteratorIn.next();
                Geometry geometryIn = (Geometry) featureIn.getDefaultGeometry();

                if (!geometry.intersects(geometryIn)) {
                    continue;
                }

                Geometry res = geometry.intersection(geometryIn);

                Geometry subGeo = null;
                for (int i = 0; i < res.getNumGeometries(); i++) {
                    subGeo = res.getGeometryN(i);
                    if (subGeo instanceof GeometryCollection) {
                        throw new IllegalStateException("ERROR : subGeo instanceof GeometryCollection");
                    }

                    newFeatures.add(KarGISFeatureFactory.getInstance().createFeature(subGeo));
                }
            }
        }
        if (newFeatures.isEmpty()) {
            return null;
        }
        return new ListFeatureCollection(newFeatures.get(0).getFeatureType(), newFeatures);
    }

    /**
     * intersect a feature with a collection of features
     * @param feature single feature
     * @param featuresIn collection of features
     * @return collection of basic features with reclculated length, area values
     */
    public SimpleFeatureCollection intersect(SimpleFeature feature, SimpleFeatureCollection featuresIn) {
        return intersect((Geometry) feature.getDefaultGeometry(), featuresIn);
    }

    /**
     * intersect a geometry with a collection of features
     * @param geometry single geometry
     * @param featuresIn collection of features
     * @return collection of basic features with reclculated length, area values
     */
    public SimpleFeatureCollection intersect(Geometry geometry, SimpleFeatureCollection featuresIn) {
        List<SimpleFeature> newFeatures = new ArrayList<SimpleFeature>();

        CoordinateReferenceSystem crs = featuresIn.getSchema().getCoordinateReferenceSystem();
        KarGISFeatureFactory.getInstance().changeCRS(crs);

        SimpleFeatureIterator iteratorIn = featuresIn.features();
        while (iteratorIn.hasNext()) {
            SimpleFeature featureIn = iteratorIn.next();
            Geometry geometryIn = (Geometry) featureIn.getDefaultGeometry();

            if (!geometry.intersects(geometryIn)) {
                continue;
            }

            Geometry res = geometry.intersection(geometryIn);

            Geometry subGeo = null;
            for (int i = 0; i < res.getNumGeometries(); i++) {
                subGeo = res.getGeometryN(i);
                if (subGeo instanceof GeometryCollection) {
                    throw new IllegalStateException("ERROR : subGeo instanceof GeometryCollection");
                }

                newFeatures.add(KarGISFeatureFactory.getInstance().createFeature(subGeo));
            }
        }

        if (newFeatures.isEmpty()) {
            return null;
        }
        return new ListFeatureCollection(newFeatures.get(0).getFeatureType(), newFeatures);
    }

    /**
     * difference of two collections of features
     * @param features collection of features
     * @param featuresIn collection of features
     * @return collection of basic features with reclculated length, area values
     */
    public SimpleFeatureCollection difference(SimpleFeatureCollection features, SimpleFeatureCollection featuresIn) {
        List<SimpleFeature> newFeatures = new ArrayList<SimpleFeature>();

        SimpleFeatureIterator iterator = features.features();
        while (iterator.hasNext()) {
            SimpleFeature feature = iterator.next();
            Geometry geometry = (Geometry) feature.getDefaultGeometry();

            SimpleFeatureIterator iteratorIn = featuresIn.features();
            while (iteratorIn.hasNext()) {
                SimpleFeature featureIn = iteratorIn.next();
                Geometry geometryIn = (Geometry) featureIn.getDefaultGeometry();

                if (!geometry.intersects(geometryIn)) {
                    continue;
                }

                Geometry res = geometry.difference(geometryIn);

                Geometry subGeo = null;
                for (int i = 0; i < res.getNumGeometries(); i++) {
                    subGeo = res.getGeometryN(i);
                    if (subGeo instanceof GeometryCollection) {
                        throw new IllegalStateException("ERROR : subGeo instanceof GeometryCollection");
                    }

                    newFeatures.add(KarGISFeatureFactory.getInstance().createFeature(subGeo));
                }
            }
        }
        if (newFeatures.isEmpty()) {
            return null;
        }
        return new ListFeatureCollection(newFeatures.get(0).getFeatureType(), newFeatures);
    }

    /**
     * difference between a feature and a collection of features
     * @param feature single feature
     * @param featuresIn collection of features
     * @return collection of basic features with reclculated length, area values
     */
    public SimpleFeatureCollection difference(SimpleFeature feature, SimpleFeatureCollection featuresIn) {
        List<SimpleFeature> newFeatures = new ArrayList<SimpleFeature>();

        Geometry geometry = (Geometry) feature.getDefaultGeometry();
        CoordinateReferenceSystem crs = featuresIn.getSchema().getCoordinateReferenceSystem();
        KarGISFeatureFactory.getInstance().changeCRS(crs);

        SimpleFeatureIterator iteratorIn = featuresIn.features();
        while (iteratorIn.hasNext()) {
            SimpleFeature featureIn = iteratorIn.next();
            Geometry geometryIn = (Geometry) featureIn.getDefaultGeometry();

            if (!geometry.intersects(geometryIn)) {
                continue;
            }

            Geometry res = geometry.difference(geometryIn);

            Geometry subGeo = null;
            for (int i = 0; i < res.getNumGeometries(); i++) {
                subGeo = res.getGeometryN(i);
                if (subGeo instanceof GeometryCollection) {
                    throw new IllegalStateException("ERROR : subGeo instanceof GeometryCollection");
                }

                newFeatures.add(KarGISFeatureFactory.getInstance().createFeature(subGeo));
            }
        }

        if (newFeatures.isEmpty()) {
            return null;
        }
        return new ListFeatureCollection(newFeatures.get(0).getFeatureType(), newFeatures);
    }

    /**
     * creates an area around a collection of features
     * @param features
     * @param distance
     * @return collection of basic features with reclculated length, area values
     */
    public SimpleFeatureCollection bufferLayer(SimpleFeatureCollection features, double distance) {
        List<SimpleFeature> newFeatures = new ArrayList<SimpleFeature>();

        SimpleFeatureIterator iterator = features.features();
        while (iterator.hasNext()) {
            SimpleFeature feature = iterator.next();
            Geometry geometry = (Geometry) feature.getDefaultGeometry();

            Geometry newGeom = geometry.buffer(distance);
            if (geometry == null) {
                continue;
            }
            try {
                for (int i = 0; i < newGeom.getNumGeometries(); i++) {
                    Geometry subGeo = newGeom.getGeometryN(i);
                    if (subGeo instanceof GeometryCollection) {
                        throw new IllegalStateException("ERROR : subGeo instanceof GeometryCollection");
                    }

                    newFeatures.add(KarGISFeatureFactory.getInstance().createFeature(subGeo));
                }
            } catch (Exception e) {
                System.out.println("Strange geom: " + newGeom.getGeometryType() + "\n" + e.getMessage());
            }
        }
        if (newFeatures.isEmpty()) {
            return null;
        }
        return new ListFeatureCollection(newFeatures.get(0).getFeatureType(), newFeatures);
    }

    /**
     * unites a collection of features to single geometry
     * @param geometryCollection
     * @return single geometry
     */
    static Geometry uniteIntoOneGeometry(Collection<Geometry> geometryCollection ) {
        Geometry all = null;
        for (Iterator<Geometry> i = geometryCollection.iterator(); i.hasNext(); ) {
            Geometry geometry = i.next();
            if (geometry == null) continue;
            if (all == null) {
                all = geometry;
            } else {
                all = all.union(geometry);
            }
        }
        return all;
    }

    /**
     * unites a collection of features to a single geometry
     * @param geometryCollection
     * @return single geometry
     */
    public Geometry uniteIntoOneGeometry(SimpleFeatureCollection collection ) {
        Geometry all = null;
        for (SimpleFeatureIterator i = collection.features(); i.hasNext(); ) {
            Geometry geometry = (Geometry) i.next().getDefaultGeometry();
            if (geometry == null) continue;
            if (all == null) {
                all = geometry;
            } else {
                all = all.union(geometry);
            }
        }
        return all;
    }

    /**
     * unites a collection of geometries to a single geometry
     * @param geometryCollection
     * @return single geometry
     */
    public Geometry uniteIntoOneGeometry(Geometry collection) {
        List<Geometry> all = new ArrayList<Geometry>();
        for (int i = 0; i < collection.getNumGeometries(); i++) {
            Geometry subGeo = collection.getGeometryN(i);
            if (subGeo instanceof GeometryCollection) {
                throw new IllegalStateException("ERROR : subGeo instanceof GeometryCollection");
            }
            all.add(subGeo);
        }
        return uniteIntoOneGeometry(all);
    }

    /**
     * difference geometry of two geometries
     * @param geom1
     * @param geom2
     * @return
     */
    public Geometry difference(Geometry geom1, Geometry geom2) {
        if (geom1 == null) return geom2;
        if (geom2 == null) return geom1;
        return uniteIntoOneGeometry(geom1.difference(geom2));
    }

    /**
     * merges two feature collections to single collection of basic features
     * @param collection1
     * @param collection2
     * @return
     */
    public SimpleFeatureCollection union(SimpleFeatureCollection collection1, SimpleFeatureCollection collection2) {
        List<SimpleFeature> newFeatures = new ArrayList<SimpleFeature>();

        for (SimpleFeatureIterator i = collection1.features(); i.hasNext(); ) {
            geomToFeatures(newFeatures, (Geometry) i.next().getDefaultGeometry());
        }
        for (SimpleFeatureIterator i = collection2.features(); i.hasNext(); ) {
            geomToFeatures(newFeatures, (Geometry) i.next().getDefaultGeometry());
        }

        return new ListFeatureCollection(newFeatures.get(0).getFeatureType(), newFeatures);
    }

    /**
     * uses SumVisitor to calculate sum of a field in feature collection
     * @param features
     * @param fieldName
     * @return sum of field
     * @throws IOException
     */
    public double getSum(SimpleFeatureCollection features, String fieldName) throws IOException {
        if (features == null) {
            return 0;
        }
        FeatureCalc calc = new SumVisitor(fieldName, features.getSchema());
        features.accepts(calc, new NullProgressListener());

        double sum = calc.getResult().toDouble();
        output("Sum calculated: " + sum);
        return sum;
    }

    /**
     * display simple table frame
     * @param model table model
     * @param title table name - frame name
     */
    public void displayTable(T2ResultTableModel model, String title) {
        JTable table = new JTable(model);

        JFrame frame = new JFrame(title);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.add(new JScrollPane(table));
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    /**
     * Create displayable layer from SimpleFeatureCollection
     * @param collection
     * @return
     */
    public Layer createLayer(SimpleFeatureCollection collection) {
        Style style = SLD.createSimpleStyle(collection.getSchema());
        return new FeatureLayer(collection, style);
    }

    /**
     * Create displayable layer from Single geometry
     * @param geometry
     * @return
     */
    public Layer createLayer(Geometry geometry) {
        ArrayList<SimpleFeature> newFeatures = new ArrayList<SimpleFeature>();
        geomToFeatures(newFeatures, geometry);
        SimpleFeatureCollection col = new ListFeatureCollection(newFeatures.get(0).getFeatureType(), newFeatures);
        return createLayer(col);
    }

    /**
     * Adds geometry and all sub-geometries to a list
     * @param list
     * @param geometry
     * @return
     */
    public List<SimpleFeature> geomToFeatures(List<SimpleFeature> list, Geometry geometry) {
        for (int i = 0; i < geometry.getNumGeometries(); i++) {
            Geometry subGeo = geometry.getGeometryN(i);
            if (subGeo instanceof GeometryCollection) {
                throw new IllegalStateException("ERROR : subGeo instanceof GeometryCollection");
            }

            list.add(KarGISFeatureFactory.getInstance().createFeature(subGeo));
        }
        return list;
    }

}
