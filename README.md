# KarGIS #

GIS application using Geotools library.
Made for Vilnius University MIF GIS course for Software Engineering students.

### Set up ###

Simply import to IDE as Maven project

### Dependencies ###

Geotools (managed by Maven)

#### T2 dependencies ####

T2 requires some GDB10LT shapefiles and some administrative unit shapefile

``` java
RES_PLOTAI_PATH = "resource/T2res/PLOTAI_sel.shp";
RES_HIDRO_L_PATH = "resource/T2res/HIDRO_L_sel.shp";
RES_KELIAI_PATH = "resource/T2res/KELIAI_sel.shp";
RES_PASTAT_P_PATH = "resource/T2res/PASTAT_P_sel.shp";
```

#### T3 dependencies ####

T3 requires some GDB10LT shapefiles

``` java
RES_KELIAI_URI = "resource/T3res/KELIAI.shp";
RES_HIDRO_L_URI = "resource/T3res/HIDRO_L.shp";
RES_GYVENVIETES_URI = "resource/T3res/VIETOV_U.shp";
RES_VIRSUKALNES_URI = "resource/T3res/virsukal.shp";
RES_PLOTAI_URI = "resource/T3res/PLOTAI.shp";
```

### Author ###

* Karolis Jocevičius
* karolis.jocevicius@gmail.com